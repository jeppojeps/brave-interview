from os import listdir
from os.path import isfile, join
import sys
import os

def listdir_fullpath(d):
    return [os.path.join(d, f) for f in os.listdir(d)]

a = open("files.txt", "w")

for i in listdir_fullpath(sys.argv[1]):
  a.write(i+'\n')

