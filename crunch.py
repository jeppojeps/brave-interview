import sys
import numpy as np

l_g = open(sys.argv[1])
l_r = open(sys.argv[2])


def getblockedarray(lines):
  res = []
  for l in lines:
    n = int(l.split(',')[-1])
    if n != 0:
      res.append(n)
  return res

glines = l_g.readlines()
rlines = l_r.readlines()
g = getblockedarray(glines)
r = getblockedarray(rlines)

a = np.array(g)
b = np.array(r)
print "global median ", np.median(a)
print "global mean", np.mean(a)
print "regional median ", np.median(b)
print "regional mean", np.mean(b)
g.sort()
r.sort()

import matplotlib.pyplot as plt

if sys.argv[3] == 'g':
  plt.hist(g, normed=True, cumulative=True, label='CDF', histtype='step', alpha=0.8,
    color='k')
  plt.savefig('global.png')
else:
  plt.hist(r, normed=True, cumulative=True, label='CDF', histtype='step', alpha=0.8,
      color='k')
  plt.savefig('regional.png')
