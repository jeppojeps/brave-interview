/* Copyright (c) 2015 Brian R. Bondy. Distributed under the MPL2 license.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include <algorithm>
#include <cerrno>
#include <iostream>
#include <vector>
#include <sstream>
#include <fstream>
#include <string>
#include "./ad_block_client.h"

using std::cout;
using std::endl;
using std::string;

const char * globaldomain;


string getFileContents(const char *filename) {
  std::ifstream in(filename, std::ios::in);
  if (in) {
    std::ostringstream contents;
    contents << in.rdbuf();
    in.close();
    return(contents.str());
  }
  throw(errno);
}

void writeFile(const char *filename, const char *buffer, int length) {
  std::ofstream outFile(filename, std::ios::out | std::ios::binary);
  if (outFile) {
    outFile.write(buffer, length);
    outFile.close();
    return;
  }
  throw(errno);
}

int checkForClient(AdBlockClient *pClient, const char *outputPath,
    const std::vector<std::string> &urlsToCheck) {
  AdBlockClient &client = *pClient;

  // This is the site who's URLs are being checked, not the domain of the
  // URL being checked.
  const char *currentPageDomain = globaldomain;
	int matched = 0;
	int matchedb = 0;

  // Do the checks
  std::for_each(urlsToCheck.begin(), urlsToCheck.end(),
      [&client, currentPageDomain, &matched](std::string const &urlToCheck) {
    if (client.matches(urlToCheck.c_str(),
          FONoFilterOption, currentPageDomain)) {
			matched++;
    }   
	});

  int size;
  // This buffer is allocate on the heap, you must call delete[] when
  // you're done using it.
  char *buffer = client.serialize(&size);
  writeFile(outputPath, buffer, size);

  AdBlockClient client2;
  // Deserialize uses the buffer directly for subsequent matches, do not free
  // until all matches are done.
  if (!client2.deserialize(buffer)) {
    cout << "Could not deserialize";
    delete[] buffer;
    return 0;
  }
  // Prints the same as client.matches would
  std::for_each(urlsToCheck.begin(), urlsToCheck.end(),
      [&client2, currentPageDomain, &matchedb](std::string const &urlToCheck) {
    if (client2.matches(urlToCheck.c_str(),
          FONoFilterOption, currentPageDomain)) {
      matchedb++;
    }  });
  delete[] buffer;
  return matched;
}

const std::vector<string> explode(const string& s, const char& c)
{
  std::string buff{""};
  std::vector<string> v;
	
	for(auto n:s)
	{
		if(n != c) buff+=n; else
		if(n == c && buff != "") { v.push_back(buff); buff = ""; }
	}
	if(buff != "") v.push_back(buff);
	
	return v;
}

int main(int argc, char**argv) {
  std::ofstream myfile;
  myfile.open ("/tmp/results.txt");

  std::string && easyListTxt = getFileContents("./test/data/easylist.txt");
  std::string && easyPrivacyTxt =
    getFileContents("./test/data/easyprivacy.txt");
  std::string && easyit =
    getFileContents("/home/ubuntu/easych.txt");
  // Parse filter lists for adblock
  AdBlockClient adBlockClient;
  adBlockClient.parse(easyListTxt.c_str());
  adBlockClient.parse(easyit.c_str());
  adBlockClient.parse(easyPrivacyTxt.c_str());
  std::vector<std::string> checkVector;
  std::ifstream fs(argv[1]);
  std::fstream fs_data;
	std::vector<std::string> files;

  if(!fs) {
      std::cerr << "file of files cannot be opened: " << argv[1] << endl;
  }
  std::string nline;
  int i = 0;
  while(std::getline(fs, nline))
  {
		files.push_back(nline);
    i++;
  }

	for (auto f : files) {
  	auto domain = f;
		std::ifstream inputTxt(domain.c_str());
    std::vector<string> v{explode(domain, '/')};
		globaldomain = v[v.size() - 1].c_str();

    if(!inputTxt) {
      std::cerr << "Data file could not be opened: " << f << endl;
    }
    std::string line; 
  	int i = 0;
  	while(std::getline(inputTxt,line))
  	{
			checkVector.push_back(line);
    	i++;
  	}
  	auto out = checkForClient(&adBlockClient, "./ABPFilterClientData.dat", checkVector);
  	myfile << globaldomain << "," << i << "," << out << endl;
    out = 0;
    i = 0;
    checkVector.clear();
    inputTxt.close();
	}
  myfile.close();
  return 0;

  // Parse filter lists for malware
  // AdBlockClient malwareClient;
  // malwareClient.parse(spam404MainBlacklistTxt.c_str());
  // malwareClient.parse(disconnectSimpleMalwareTxt.c_str());
  // std::vector<std::string> checkVector2;
  // checkVector2.push_back("http://freexblcode.com/test");
  // checkVector2.push_back("https://malware-check.disconnect.me");
  // checkVector2.push_back("http://www.brianbondy.com");
  // checkForClient(&malwareClient, "./SafeBrowsingData.dat", checkVector2);

  return 0;
}
