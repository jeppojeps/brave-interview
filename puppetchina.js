// save as index.js
// npm install https://github.com/GoogleChrome/puppeteer/
// node index.js URL
const puppeteer = require('puppeteer');
const fs = require('fs');
(async () => {
  const url = 'http://'+process.argv[2];
  console.log(url);
  const browser = await puppeteer.launch({headless:true, timeout:30000});
try {
  // use tor
  //const browser = await puppeteer.launch({args:['--proxy-server=socks5://127.0.0.1:9050']});
  const page = await browser.newPage();
  await page.goto(url, {waitUntil: 'load'});

  //const title = await page.title();
  //console.log(title);
  const hrefs = await page.evaluate(() => {
      const anchors = document.querySelectorAll('a');
      return [].map.call(anchors, a => a.href);
  });
  const imgsrc = await page.evaluate(() => {
      const anchors = document.querySelectorAll('img');
      return [].map.call(anchors, img => img.src);
  });
  const scsrc = await page.evaluate(() => {
      const anchors = document.querySelectorAll('script');
      return [].map.call(anchors, script => script.src);
  });

  fs.writeFile('/media/rover/chinaresult/'+process.argv[2], hrefs+imgsrc+scsrc);
  browser.close();
} catch (err){
  browser.close();
}
})();
